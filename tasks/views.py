from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from tasks.models import Task


# Create your views here.
class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "projects/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    success_url = reverse_lazy("show_project")


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ["is_complete"]
    success_url = reverse_lazy("show_my_tasks")
